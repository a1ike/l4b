$(document).ready(function () {

  $('form').submit(function (e) {
    e.preventDefault();
    var f = $(this);
    $('.ierror', f).removeClass('ierror');
    var phone = $('input[name="phone"]', f).val();
    var error = false;
    if (phone == '') {
      $('input[name="phone"]', f).addClass('ierror');
      error = true;
    }
    if (error) {
      return false;
    }
    $.ajax({
      type: 'POST',
      url: 'mail.php',
      data: $(this).serialize(),
    }).done(function (data) {
      if (data.response == 'ok') {
        alert('Спасибо! Ваша заявка отправлена');
        /* window.location.href = '../order.php'; */
      }
      else {
        alert('Ошибка! Заявка не отправлена, повторите запрос позже');
      }
    });
    return false;
  });

});
